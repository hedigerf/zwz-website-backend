import { createLogger, format, transports, Logger } from 'winston'
import DailyRotateFile from 'winston-daily-rotate-file'
import { express, LoggingWinston } from '@google-cloud/logging-winston'

const isGCP = Boolean(process.env.GAE_INSTANCE)
const loggingFormat = format.combine(
	format.timestamp(),
	format.printf((i) => `${i.timestamp} | ${i.module} | ${i.message}`)
)
const gcpTransport = new LoggingWinston()
const logger = createLogger({
	level: isGCP ? 'info' : 'debug',
	format: loggingFormat,
	transports: [
		new transports.Console(),
		isGCP ? gcpTransport : new DailyRotateFile({ filename: 'zwz-website-backend.log' }),
	],
})

logger.child({ module: 'logger' }).debug(`Logger initialized with isGCP=${isGCP}`)

function getLogger(options: Record<string, unknown>): Logger {
	return logger.child(options)
}

export const gcpWinstonMiddleware = express.makeMiddleware(logger, gcpTransport)

export default getLogger
