import express from 'express'
import getLogger from '../../logger/logger'
import { AwarnessService } from '../../services/awarness/awarness.service'
import { Incident } from './awarness.routes.d'
import { check, body } from 'express-validator'
import { validate } from '../base'

export const awarnessBaseUrl = '/api/awarness'

const logger = getLogger({ module: 'awarness.routes' })

export function initAwarnessRouter(awarnessService: AwarnessService): express.Router {
	const router = express.Router()
	router.post(
		'/incidents',
		body('name').optional().trim().escape(),
		body('email').optional().isEmail().normalizeEmail(),
		body('message').notEmpty().trim().escape(),
		check('message').notEmpty(),
		async (req: express.Request, resp) => {
			try {
				validate(logger, req)
				const incidentJson: Incident = req.body
				logger.info(`Incident Received ${JSON.stringify(incidentJson)}`)
				await awarnessService.reportIncident(incidentJson)
				resp.send(incidentJson)
			} catch (error) {
				logger.error(error)
				resp.sendStatus(500)
			}
		}
	)
	return router
}
