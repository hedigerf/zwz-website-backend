import express from 'express'
import { validationResult } from 'express-validator'
import { List } from 'immutable'
import winston from 'winston'

export function validate(logger: winston.Logger, req: express.Request): void {
	const valRes = validationResult(req)
	List(valRes.array()).forEach((valError) => logger.warn(`${valError.param}: ${valError.msg}`))
	valRes.throw()
}
