import express from 'express'
import { header, query, validationResult } from 'express-validator'
import getLogger from '../../logger/logger'
import { EventsService } from '../../services/events/events.service'
import { EventTO, EventSearchSortableAttribute, Direction } from '../../services/events/events.service.d'
import { List } from 'immutable'
import { Event, Events, PartialEvent } from './events.routes.d'
import { LanguageHolderTO } from '../../services/types'
import { Response } from 'express-serve-static-core'

type Lang = keyof LanguageHolderTO
type EventFilterQuery = { lang?: Lang; from?: string; to?: string; direction?: string; eventTypes?: string }

const defaultLang: Lang = 'en'

const logger = getLogger({ module: 'events.routes' })
export const eventsBaseUrl = '/api/events'

function convertToEvent(lang: Lang): (event: EventTO) => Event {
	return (event: EventTO) => ({
		_links: {
			self: { href: `${eventsBaseUrl}/${event.id}` },
			next: { href: `${eventsBaseUrl}/${event.id}/next` },
			previous: { href: `${eventsBaseUrl}/${event.id}/previous` },
			images: [{ href: `/api/images/${event.id}` }],
		},
		id: event.id,
		title: event.title,
		fromDate: event.fromDate,
		toDate: event.toDate,
		// eslint-disable-next-line security/detect-object-injection
		type: event.type[lang] as string,
		// eslint-disable-next-line security/detect-object-injection
		tags: event.tags[lang] as Array<string>, // eslint-disable-next-line security/detect-object-injection
		description: event.description[lang] as string,
		lineup: event.lineup,
		location: event.location,
		ticketUrl: event.ticketUrl,
		imgUrls: event.imgUrls.length > 0 ? [{ href: `/api/images/${event.id}` }] : [],
	})
}

function createEventsResponse(lang: Lang, events: List<EventTO>): Events {
	const partialEvents = events.map(convertToPartialEvent(lang))
	return {
		_links: { self: { href: `${eventsBaseUrl}/` } },
		_embedded: { events: partialEvents.toArray() },
	}
}

function convertToPartialEvent(lang: Lang): (event: EventTO) => PartialEvent {
	return (event: EventTO) => ({
		_links: {
			self: { href: `${eventsBaseUrl}/${event.id}` },
			image: { href: `/api/images/${event.id}` },
		},
		id: event.id,
		title: event.title,
		fromDate: event.fromDate,
		toDate: event.toDate,
		// eslint-disable-next-line security/detect-object-injection
		type: event.type[lang] as string,
		// eslint-disable-next-line security/detect-object-injection
		tags: event.tags[lang] as Array<string>,
		imgUrl: event.imgUrls.length > 0 ? `/api/images/${event.id}` : undefined,
		location: event.location,
	})
}

export function initEventsRouter(eventsService: EventsService): express.Router {
	const router = express.Router()
	router.get(
		'/',
		query('from').optional().isNumeric(),
		query('to').optional().isNumeric(),
		query('direction').optional().isIn(['ascending', 'descending']),
		async (
			req: express.Request<Record<string, unknown>, unknown, unknown, EventFilterQuery>,
			resp: express.Response
		) => {
			try {
				validationResult(req).throw()
				const { lang, from, to, eventTypes, direction }: EventFilterQuery = req.query
				logger.debug(
					`Received EventFilterQuery: lang=${lang} from=${from} to=${to} direction=${direction} eventTypes=${eventTypes}`
				)
				const foundDirection = direction as Direction
				if (from && to && foundDirection) {
					const exampleFilterEntry: EventSearchSortableAttribute = {
						key: 'fromDate',
						range: {
							from: Number(from),
							to: Number(to),
						},
						type: 'withinRange',
						direction: foundDirection,
					}
					const events = await eventsService.fetch(List([exampleFilterEntry]))
					answerWithLanguage(lang, resp, events)
				} else if (foundDirection) {
					const filterEntry: EventSearchSortableAttribute = {
						key: 'fromDate',
						value: from ? Number(from) : to ? Number(to) : undefined,
						direction: foundDirection,
						type: from ? 'greaterThanOrEqual' : to ? 'lessThanOrEqual' : undefined,
					}
					const events = await eventsService.fetch(List([filterEntry]))
					answerWithLanguage(lang, resp, events)
				} else {
					const events = await eventsService.fetch(List())
					answerWithLanguage(lang, resp, events)
				}
			} catch (error) {
				logger.error(error)
				resp.sendStatus(500)
			}
		}
	)
	router.get('/types', async (_, resp) => {
		resp.send('hello')
	})
	router.get('/:id/next', async (_, resp) => {
		resp.sendStatus(501)
	})
	router.get('/:id/previous', async (_, resp) => {
		resp.sendStatus(501)
	})
	const syncToken = process.env.SYNC_TOKEN
	if (syncToken) {
		router.get('/sync', header('sync-token').trim().escape().equals(syncToken), async (req, resp) => {
			try {
				logger.debug(`Received events sync request from ${req.ip}`)
				validationResult(req).throw()
				const events = await eventsService.syncEvents()
				resp.type('application/hal+json').send(events.map(convertToEvent('en')))
			} catch (error) {
				logger.error(error)
				resp.sendStatus(500)
			}
		})
	}

	router.get('/:id', async (req, resp) => {
		try {
			const res = await eventsService.fetchById(req.params.id)
			const { lang }: { lang?: Lang } = req.query
			if (lang) {
				resp.type('application/hal+json').send(convertToEvent(lang)(res))
			} else {
				resp.type('application/hal+json').send(convertToEvent(defaultLang)(res))
			}
		} catch (error) {
			logger.error(error)
			resp.sendStatus(500)
		}
	})

	return router
}
function answerWithLanguage(
	lang: string | undefined,
	resp: Response<unknown, Record<string, unknown>, number>,
	events: List<EventTO>
) {
	if (lang) {
		resp.type('application/hal+json').send(createEventsResponse(lang as Lang, events))
	} else {
		resp.type('application/hal+json').send(createEventsResponse(defaultLang, events))
	}
}
