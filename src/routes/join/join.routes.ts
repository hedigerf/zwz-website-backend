import express from 'express'
import getLogger from '../../logger/logger'
import { check, body } from 'express-validator'
import { JoinRequest } from './join.routes.d'
import { JoinService } from '../../services/join/join.service'
import { validate } from '../base'

export const joinBaseUrl = '/api/join'

const logger = getLogger({ module: 'join.routes' })

export function initJoin(joinService: JoinService): express.Router {
	const router = express.Router()
	router.post(
		'/',
		body('name').notEmpty().trim().escape(),
		body('email').notEmpty().isEmail().normalizeEmail(),
		body('message').notEmpty().trim().escape(),
		check('message').notEmpty(),
		check('email').notEmpty(),
		check('message').notEmpty(),
		async (req: express.Request, resp) => {
			try {
				validate(logger, req)
				const joinRequest: JoinRequest = req.body
				logger.info(`JoinRequest Received ${JSON.stringify(joinRequest)}`)
				await joinService.reportJoinRequest(joinRequest)
				resp.send(joinRequest)
			} catch (error) {
				logger.error(error)
				resp.sendStatus(500)
			}
		}
	)
	return router
}
