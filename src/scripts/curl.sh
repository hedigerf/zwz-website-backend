#awarness
curl -X POST -H "Content-Type: application/json" \
    -d '{"name": "Lorena", "message": " this is a test message. ", "email": "lorena@example.com"}' \
    http://localhost:8081/api/awarness/incidents

curl -X POST -H "Content-Type: application/json" \
    -d '{}' \
    http://localhost:8081/api/awarness/incidents

#join
curl -X POST -H "Content-Type: application/json" \
    -d '{"name": "Joiner", "message": "I want to join.", "email": "joiner@example.com"}' \
    http://localhost:8081/api/join

#events sync
curl -X GET -H "sync-token: xxx" http://localhost:8081/api/events/sync

#events filter
curl -X GET http://localhost:8081/api/events?lang=en&from=0&to=9999999999999&direction=ascending

curl -X GET http://localhost:8081/api/events/?lang=de&direction=ascending&from=0&to=163699

curl -X GET http://localhost:8081/api/events/?lang=de&direction=ascending&from=1638222979

#twitch

curl -X GET 'http://localhost:8081/api/streaming'

curl -X GET 'https://api.twitch.tv/helix/users?login=twitchdev' \
-H 'Authorization: Bearer xxx' \
-H 'Client-Id: xxx'

curl -X GET 'https://api.twitch.tv/helix/streams' \
-H 'Authorization: Bearer xxx' \
-H 'Client-Id: xxx'

curl -X GET 'https://api.twitch.tv/helix/streams?user_login=forsen' \
-H 'Authorization: Bearer xxx' \
-H 'Client-Id: xxx'

curl -X GET 'https://api.twitch.tv/helix/streams?user_login=zentralwaescherei' \
-H 'Authorization: Bearer xxx' \
-H 'Client-Id: xxx'

