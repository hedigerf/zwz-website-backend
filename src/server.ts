import express from 'express'
import initFirestore from './startup/firestore.startup'
import { initEventsRouter, eventsBaseUrl } from './routes/events/events.routes'
import getLogger, { gcpWinstonMiddleware } from './logger/logger'
import initEventService from './startup/events.startup'
import { awarnessBaseUrl, initAwarnessRouter } from './routes/awarness/awarness.routes'
import { initAwarnessService } from './startup/awarness.startup'
import initCodaIntegrationService from './startup/coda.startup'
import { ImageConversionService } from './services/images/image-conversion.service'
import { initJoinService } from './startup/join.startup'
import { initJoin, joinBaseUrl } from './routes/join/join.routes'
import { initStreaming, streamingBaseUrl } from './routes/streaming/streaming.routes'
import { tracer } from './startup/tracing.startup'

async function main(): Promise<void> {
	tracer
	const logger = getLogger({ module: 'server' })
	const app = express()
	const port = process.env.PORT || 8081

	app.use(await gcpWinstonMiddleware)

	initFirestore()

	app.use(express.json())

	const codaIntegrationService = initCodaIntegrationService()
	const eventService = initEventService(codaIntegrationService)
	const imageConversionService = new ImageConversionService()

	app.use(eventsBaseUrl, initEventsRouter(eventService))
	app.use(awarnessBaseUrl, initAwarnessRouter(initAwarnessService()))
	app.use(joinBaseUrl, initJoin(initJoinService()))
	app.use(streamingBaseUrl, initStreaming())

	//pass eventId
	app.use('/api/images/:id', async (req, resp) => {
		try {
			const eventId = req.params.id
			const {
				imgUrls: [firstImgUrl],
			} = await eventService.fetchById(eventId)
			if (firstImgUrl) {
				const buffer = await codaIntegrationService.fetchImage(firstImgUrl)
				const converted = await imageConversionService.convertImage(buffer)
				resp.setHeader('Content-Type', 'image/jpg')
				resp.setHeader('Content-Length', converted.length)
				resp.send(converted)
			} else {
				logger.warn(`No image found for id ${eventId}`)
				resp.sendStatus(404)
			}
		} catch (error) {
			logger.error(error)
			resp.sendStatus(500)
		}
	})
	app.listen(port, () => {
		logger.info(`Listening on port ${port}...`)
	})

	app.get('/_ah/warmup', (_, resp) => {
		logger.info('warm-up request received.')
		resp.sendStatus(200)
	})
}
main()
