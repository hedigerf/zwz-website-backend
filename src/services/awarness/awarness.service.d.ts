export interface IncidentTO {
	name?: string
	email?: string
	message: string
}
