import { Coda } from 'coda-js'
import getLogger from '../logger/logger'
import ServiceError from './service-error'
import { EventTO } from './events/events.service.d'
import { List } from 'immutable'
import moment from 'moment'
import { AxiosStatic } from 'axios'
import { tracer } from '../startup/tracing.startup'

const logger = getLogger({ module: 'coda-integration.service' })
const codaDateFormat = 'YYYY-MM-DDTHH:mm:ss.SSSZZ'

export default class CodaIntegrationService {
	private readonly requiredColumns: List<string> = List.of(
		'title',
		'from_date',
		'to_date',
		'type_de',
		'type_en',
		'location',
		'publish',
		'last_modified'
	)

	constructor(
		private readonly coda: Coda,
		private readonly documentId: string,
		private readonly httpClient: AxiosStatic
	) {}

	async fetchImage(imgUrl: string): Promise<Buffer> {
		const span = tracer.createChildSpan({ name: 'fetch-coda-image' })
		try {
			const start = Date.now()
			const img = await this.httpClient.get(imgUrl, { responseType: 'arraybuffer' })
			const millis = Date.now() - start
			logger.debug(`Fetched image (${imgUrl}) with status ${img.status}, it took ${millis}ms`)
			const buffer = Buffer.from(img.data, 'binary')
			span.endSpan()
			return buffer
		} catch (err) {
			logger.error(err)
			span.endSpan()
			throw new ServiceError(`Coudn't fetch picture from coda.`)
		}
	}

	async listTables(): Promise<void> {
		const doc = await this.coda.getDoc(this.documentId)
		const tables = await doc.listTables()
		tables.forEach((tbl) => logger.info(tbl.id))
	}

	async fetchEvents(): Promise<List<EventTO>> {
		try {
			const start = Date.now()
			const eventTbl = await this.coda.getTable(this.documentId, 'grid-pmpFLwkaWc')
			const codaColumns = List(await eventTbl.listColumns({ useColumnNames: true }))
			const valid = this.requiredColumns.every((requiredColumn) =>
				codaColumns.some((codaColumn) => requiredColumn === (codaColumn as any).name)
			)
			if (!valid) {
				throw new ServiceError('Abort synchronization, minimal required columns not available from Coda.')
			}
			const rows = List(await eventTbl.listRows({ useColumnNames: true }))
			const events = rows.reduce((acc, row) => {
				const codaId = row.id as string
				const title = row.values.title as string
				const fromDate = row.values.from_date as string
				const toDate = row.values.to_date as string
				const lastModified = row.values.last_modified as string
				const publish = row.values.publish as boolean
				if (!publish || !codaId || !title || title === '' || !fromDate || !toDate) {
					logger.debug(`discard ${JSON.stringify(row.values)}`)
					return acc
				}
				const fromDateConverted = moment(fromDate, codaDateFormat).unix()
				const toDateConverted = moment(toDate, codaDateFormat).unix()
				const lastModifiedConverted = moment(lastModified, codaDateFormat).unix()
				return acc.push({
					id: undefined,
					codaId: codaId,
					title: title,
					fromDate: fromDateConverted,
					toDate: toDateConverted,
					type: {
						en: row.values.type_en,
						de: row.values.type_de,
					},
					description: {
						en: row.values.description_en,
						de: row.values.description_de,
					},
					lineup: row.values.lineup.split(','),
					location: row.values.location.split(','),
					ticketUrl: row.values.ticket_url,
					host: row.values.host,
					soundengineer: row.values.soundengineer,
					imgUrls: row.values.image_url ? [row.values.image_url] : [],
					tags: {
						en: row.values.tags_en.split(','),
						de: row.values.tags_de.split(','),
					},
					lastModified: lastModifiedConverted,
				})
			}, List<EventTO>())
			const millis = Date.now() - start
			logger.info(`Fetched ${events.size} rows from coda, it took ${millis}ms.`)
			logger.debug(`Events: ${JSON.stringify(events)}`)
			return events
		} catch (error) {
			logger.error(error)
			throw new ServiceError('Fetch Events from Coda failed.')
		}
	}
}
