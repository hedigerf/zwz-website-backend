import { LanguageHolderTO } from '../types'

export interface EventsSearchFilterModel {
	lang?: Lang
	attributes: Array[EventSearchSortableAttribute]
}

export type SupportedAttribute = 'fromDate' | 'toDate' | 'type'

export type Direction = 'ascending' | 'descending'

export type FilterType = 'greaterThanOrEqual' | 'lessThanOrEqual' | 'withinRange'

export interface EventSearchSortableAttribute {
	key: SupportedAttribute
	value?: number | string
	range?: Range<number | string>
	direction?: Direction
	type?: FilterType
}

export interface Range<T> {
	from: T
	to: T
}

export interface EventTO extends CodaSyncObject {
	id?: string
	title: string
	fromDate: number
	toDate: number
	type: LanguageHolderTO
	description: LanguageHolderTO
	tags: LanguageHolderTO
	lineup: Array<string>
	location: Array<string>
	ticketUrl: string
	host: string
	soundengineer: string
	imgUrls: Array<string>
}

export interface CodaSyncObject {
	codaId: string
	lastModified: number
}
