import { EventsService } from './events.service'
import CodaIntegrationService from '../coda-integration.service'
import { IEventsRepository, PersistableEvent } from '../../storage/database/events.storage'
import chai, { expect } from 'chai'
import chaiAsPromised from 'chai-as-promised'
import { stubConstructor, stubInterface, StubbedInstance } from 'ts-sinon'
import { List } from 'immutable'
import ServiceError from '../service-error'
import { EventTO } from './events.service.d'

describe('EventsService', () => {
	let codaIntegrationService: StubbedInstance<CodaIntegrationService>
	let eventsRepository: StubbedInstance<IEventsRepository>
	let eventsService: EventsService

	const expectedId = '666'
	const expectedCodaId = 'sfds-dsdf-3qsd-5674'
	const expectedTitle = 'event of the devil'
	const expectedFromDate = 888
	const expectedToDate = 999
	const expectedType = {
		en: 'type',
		de: 'typ',
	}
	const expectedTags = {
		en: ['world music'],
		de: ['welt musik'],
	}
	const expectedDescription = {
		en: 'Description',
		de: 'Description',
	}
	const expectedLineup = ['artist1']
	const expectedLocation = ['MVR']
	const expectedTicketUrl = 'URL'
	const expectedHost = 'Host'
	const expectedSoundengineer = 'soundengineer'
	const expectedImgeUrls = ['sample.jpg']
	const expectedLastModified = 666

	const expectedEvent: EventTO = {
		id: expectedId,
		codaId: expectedCodaId,
		title: expectedTitle,
		fromDate: expectedFromDate,
		toDate: expectedToDate,
		type: expectedType,
		tags: expectedTags,
		description: expectedDescription,
		lineup: expectedLineup,
		location: expectedLocation,
		ticketUrl: expectedTicketUrl,
		host: expectedHost,
		soundengineer: expectedSoundengineer,
		imgUrls: expectedImgeUrls,
		lastModified: expectedLastModified,
	}

	const event = new PersistableEvent()
	event.id = expectedId
	event.codaId = expectedCodaId
	event.title = expectedTitle
	event.fromDate = expectedFromDate
	event.toDate = expectedToDate
	event.type = expectedType
	event.tags = expectedTags
	event.description = expectedDescription
	event.lineup = expectedLineup
	event.location = expectedLocation
	event.ticketUrl = expectedTicketUrl
	event.host = expectedHost
	event.soundengineer = expectedSoundengineer
	event.imgUrls = expectedImgeUrls
	event.lastModified = expectedLastModified

	before(() => {
		chai.use(chaiAsPromised)
	})

	beforeEach(() => {
		codaIntegrationService = stubConstructor(CodaIntegrationService)
		eventsRepository = stubInterface<IEventsRepository>()
		eventsService = new EventsService(codaIntegrationService, eventsRepository)
	})

	describe('#fetchAll', () => {
		it('should fetch without error', async () => {
			eventsRepository.search.resolves(List([event]))

			const res = await eventsService.fetch(List())

			expect(res.size).to.equal(1)
			expect(res.get(0)).to.deep.equal(expectedEvent)
		})

		it('should throw error if any issues', async () => {
			eventsRepository.search.rejects('you are dismissed')
			await expect(eventsService.fetch(List())).to.eventually.be.rejectedWith(ServiceError)
		})
	})
	describe('#fetch', () => {
		it('should fetch without error', async () => {
			eventsRepository.findById.resolves(event)
			await expect(eventsService.fetchById(expectedId)).to.eventually.deep.equal(expectedEvent)
		})
		it('should throw error if any issues', async () => {
			eventsRepository.findById.rejects('you are dismissed')
			await expect(eventsService.fetchById(expectedId)).to.eventually.be.rejectedWith(ServiceError)
		})
		it('should throw error if event not found', async () => {
			eventsRepository.findById.resolves(undefined)
			await expect(eventsService.fetchById(expectedId)).to.eventually.be.rejectedWith(ServiceError)
		})
	})
})
