import CodaIntegrationService from '../coda-integration.service'
import { IEventsRepository, PersistableEvent } from '../../storage/database/events.storage'
import { EventTO, EventSearchSortableAttribute } from './events.service.d'
import getLogger from '../../logger/logger'
import { List } from 'immutable'
import ServiceError from '../service-error'

const logger = getLogger({ module: 'events.service' })

export class EventsService {
	constructor(
		private readonly codaIntegrationService: CodaIntegrationService,
		private readonly eventsRepository: IEventsRepository
	) {}

	public async syncEvents(): Promise<List<EventTO>> {
		try {
			const codaEvents = await this.codaIntegrationService.fetchEvents()
			const start = Date.now()
			const newEvents = List(await Promise.all(codaEvents.map(this.createOrUpdate)))
			const allEvents = await this.eventsRepository.search(List())
			const toDelete = allEvents.filterNot((event) => newEvents.some((newEvent) => event.id === newEvent.id))
			logger.debug(`Found ${toDelete.size} entries to delete`)
			toDelete.forEach(async (eventToDelete) => await this.eventsRepository.delete(eventToDelete.id))
			const millis = Date.now() - start
			logger.info(`Sync without coda fetch took ${millis}ms.`)
			return newEvents.map(this.convert)
		} catch (error) {
			logger.error(error)
			throw new ServiceError('Synchronization of Events with Coda failed.')
		}
	}

	public async fetch(searchQuery: List<EventSearchSortableAttribute>): Promise<List<EventTO>> {
		try {
			const start = Date.now()
			const allEvents = await this.eventsRepository.search(searchQuery)
			const millis = Date.now() - start
			logger.info(`Fetch all Events took ${millis}ms.`)
			return allEvents.map(this.convert)
		} catch (error) {
			logger.error(error)
			throw new ServiceError('Fetch all Events failed.')
		}
	}

	public async fetchById(id: string): Promise<EventTO> {
		try {
			const event = await this.eventsRepository.findById(id)
			if (!event) {
				throw new ServiceError(`Event with Id ${id} doesn't exist.`)
			}
			return this.convert(event)
		} catch (error) {
			logger.error(error)
			throw new ServiceError(`Fetching Event with Id ${id} failed.`)
		}
	}

	private createOrUpdate = async (codaEvent: EventTO) => {
		logger.debug(`CreateOrUpdate event: ${JSON.stringify(codaEvent)}`)
		const loadedEvent = await this.eventsRepository.findByCodaId(codaEvent.codaId)
		const existAlready = loadedEvent != null
		if (!existAlready || loadedEvent.lastModified !== codaEvent.lastModified) {
			const toPersist = existAlready ? loadedEvent : new PersistableEvent()
			toPersist.codaId = codaEvent.codaId
			toPersist.title = codaEvent.title
			toPersist.fromDate = codaEvent.fromDate
			toPersist.toDate = codaEvent.toDate
			toPersist.type = codaEvent.type
			toPersist.description = codaEvent.description
			toPersist.lineup = codaEvent.lineup
			toPersist.location = codaEvent.location
			toPersist.ticketUrl = codaEvent.ticketUrl
			toPersist.host = codaEvent.host
			toPersist.soundengineer = codaEvent.soundengineer
			toPersist.imgUrls = codaEvent.imgUrls
			toPersist.tags = codaEvent.tags
			toPersist.lastModified = codaEvent.lastModified
			const persisted = await (existAlready
				? this.eventsRepository.update(toPersist)
				: this.eventsRepository.create(toPersist))
			return persisted
		}
		logger.debug(`Event (id=${loadedEvent.id}) doesn't need to be persisted. No modifications detected.`)
		return loadedEvent
	}

	private convert(persistentEvent: PersistableEvent): EventTO {
		return { ...persistentEvent }
	}
}
