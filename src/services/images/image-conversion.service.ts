import Jimp from 'jimp'
import getLogger from '../../logger/logger'
import { tracer } from '../../startup/tracing.startup'
import ServiceError from '../service-error'

const logger = getLogger({ module: 'image-conversion.service' })
const maxWidth = 450
export class ImageConversionService {
	public async convertImage(img: Buffer): Promise<Buffer> {
		const span = tracer.createChildSpan({ name: 'convert-image' })
		try {
			const start = Date.now()
			const toConvert = await Jimp.read(img)
			const converted = await this.reduceQuality(this.maybeScale(toConvert)).getBufferAsync(Jimp.MIME_JPEG)
			const millis = Date.now() - start
			logger.debug(`Image successfully converted in ${millis}ms.`)
			span.endSpan()
			return converted
		} catch (err) {
			logger.error(err)
			span.endSpan()
			throw new ServiceError(`Coudn't convert image.`)
		}
	}

	private maybeScale(img: Jimp): Jimp {
		const span = tracer.createChildSpan({ name: 'scale-image' })
		if (img.getWidth() > maxWidth) {
			const resized = img.resize(maxWidth, Jimp.AUTO)
			span.endSpan()
			return resized
		}
		span.endSpan()
		return img
	}

	private reduceQuality(img: Jimp): Jimp {
		const span = tracer.createChildSpan({ name: 'reduce-quality-image' })
		const reducedQuality = img.quality(60)
		span.endSpan()
		return reducedQuality
	}
}
