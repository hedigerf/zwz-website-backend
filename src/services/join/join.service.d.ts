export interface JoinRequestTO {
	name: string
	email: string
	message: string
}
