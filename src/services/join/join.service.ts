import { JoinRequestTO } from './join.service.d'
import { Transporter } from 'nodemailer'
import SMTPTransport from 'nodemailer/lib/smtp-transport'
import { emailSender } from '../../startup/email.startup'
import getLogger from '../../logger/logger'
import ServiceError from '../service-error'

const emailReceiver = process.env.JOIN_EMAIL_USER

const logger = getLogger({ module: 'join.service' })

export class JoinService {
	constructor(private readonly emailTransporter: Transporter<SMTPTransport.SentMessageInfo>) {}

	async reportJoinRequest(joinRequest: JoinRequestTO): Promise<string> {
		try {
			const info = await this.emailTransporter.sendMail({
				from: emailSender,
				to: emailReceiver,
				subject: `New join request from ${joinRequest.name}`,
				text: `from: ${joinRequest.name}, email: ${joinRequest.email}, message: ${joinRequest.message}`,
				html: `
				from: ${joinRequest.name}</br>
				email: ${joinRequest.email}</br>
				message: ${joinRequest.message}`,
			})
			logger.info(`Email sent ${info.messageId}`)
			return info.messageId
		} catch (error) {
			logger.error(error)
			throw new ServiceError('Email sending failed.')
		}
	}
}
