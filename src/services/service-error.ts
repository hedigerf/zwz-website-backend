export default class ServiceError extends Error {
	constructor(public message: string) {
		super(message)
		Object.setPrototypeOf(this, ServiceError.prototype)
	}
}
