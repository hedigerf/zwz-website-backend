import { AxiosStatic } from 'axios'
import getLogger from '../../logger/logger'
import ServiceError from '../service-error'

const logger = getLogger({ module: 'twitch.service' })

export class TwitchService {
	constructor(
		private readonly httpClient: AxiosStatic,
		private readonly appAccessToken: string,
		private readonly clientId: string
	) {}

	async checkIfLive(): Promise<boolean> {
		try {
			const twitchApiResult = await this.httpClient.get(
				'https://api.twitch.tv/helix/streams?user_login=zentralwaescherei',
				{
					headers: {
						Authorization: `Bearer ${this.appAccessToken}`,
						'Client-Id': this.clientId,
					},
				}
			)
			const dataArray = twitchApiResult.data.data as Array<unknown>
			logger.debug(`Retrieved response from twitch api: ${JSON.stringify(dataArray)}`)
			return dataArray != null && dataArray.length > 0
		} catch (error) {
			logger.error(error)
			throw new ServiceError('Could not check if the zw stream is online.')
		}
	}
}
