export interface LanguageHolderTO {
	en: string | Array<string>
	de: string | Array<string>
}
