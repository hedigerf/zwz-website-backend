import { AwarnessService } from '../services/awarness/awarness.service'
import { transporter } from './email.startup'

export function initAwarnessService(): AwarnessService {
	const service = new AwarnessService(transporter)
	return service
}
