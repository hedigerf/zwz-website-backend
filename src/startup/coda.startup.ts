import { Coda } from 'coda-js'
import StartUpError from './startup-error'
import CodaIntegrationService from '../services/coda-integration.service'
import axios from 'axios'

export default function initCodaIntegrationService(): CodaIntegrationService {
	const coda = initCoda()
	const zwzDocumentId = 'bGhTkNmqQM'
	return new CodaIntegrationService(coda, zwzDocumentId, axios)
}
function initCoda(): Coda {
	const codaToken = process.env.CODA_TOKEN
	if (typeof codaToken == 'undefined') {
		throw new StartUpError('Coda Token not defined.')
	}
	return new Coda(codaToken)
}
