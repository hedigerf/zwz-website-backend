import nodemailer from 'nodemailer'

const emailUser = process.env.EMAIL_USER

export const emailSender = `"ZW Website" <${emailUser}>`

const emailPassword = process.env.EMAIL_PASSWORD

export const transporter = nodemailer.createTransport({
	host: 'smtp.gmail.com',
	port: 587,
	secure: false, // true for 465, false for other ports
	auth: {
		user: emailUser, // generated ethereal user
		pass: emailPassword, // generated ethereal password
	},
})
