import { JoinService } from '../services/join/join.service'
import { transporter } from './email.startup'

export function initJoinService(): JoinService {
	const service = new JoinService(transporter)
	return service
}
