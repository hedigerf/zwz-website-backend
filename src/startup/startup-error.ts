export default class StartUpError extends Error {
	constructor(public message: string) {
		super(message)
		Object.setPrototypeOf(this, StartUpError.prototype)
	}
}
