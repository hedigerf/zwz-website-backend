import { BaseFirestoreRepository, Collection, CustomRepository, IQueryBuilder } from 'fireorm'
import { List } from 'immutable'
import { HasId, IRepository, LanguageHolder } from './types'
import { EventSearchSortableAttribute } from '../../services/events/events.service.d'

@Collection('Events')
export class PersistableEvent implements HasId<string> {
	id = ''
	codaId = ''
	title = ''
	fromDate = 0
	toDate = 0
	type: LanguageHolder = {
		en: '',
		de: '',
	}
	description: LanguageHolder = {
		en: '',
		de: '',
	}
	tags: LanguageHolder = {
		en: '',
		de: '',
	}
	lineup: Array<string> = []
	location: Array<string> = []
	ticketUrl = ''
	host = ''
	soundengineer = ''
	imgUrls: Array<string> = []
	lastModified = 0
}

export interface IEventsRepository extends IRepository<PersistableEvent, PersistableEvent['id']> {
	findByCodaId(codaId: string): Promise<PersistableEvent | null>

	search(searchQuery: List<EventSearchSortableAttribute>): Promise<List<PersistableEvent>>
}

@CustomRepository(PersistableEvent)
export class EventsRepository extends BaseFirestoreRepository<PersistableEvent> implements IEventsRepository {
	async findByCodaId(codaId: string): Promise<PersistableEvent | null> {
		return this.whereEqualTo('codaId', codaId).findOne()
	}

	private handleWithinRange(
		queryBuilder: IQueryBuilder<PersistableEvent>,
		attribute: EventSearchSortableAttribute
	): IQueryBuilder<PersistableEvent> {
		const { range, key } = attribute
		if (range) {
			const { from, to } = range
			return queryBuilder.whereGreaterOrEqualThan(key, from).whereLessOrEqualThan(key, to)
		} else {
			throw new Error('Invalid Search request.')
		}
	}

	private handleWhere(
		queryBuilder: IQueryBuilder<PersistableEvent>,
		attribute: EventSearchSortableAttribute
	): IQueryBuilder<PersistableEvent> {
		const { key, value, type } = attribute
		switch (type) {
			case 'withinRange':
				return this.handleWithinRange(queryBuilder, attribute)
			case 'greaterThanOrEqual':
				if (value !== undefined) {
					return queryBuilder.whereGreaterOrEqualThan(key, value)
				} else {
					throw new Error('Invalid Search request.')
				}
			case 'lessThanOrEqual':
				if (value) {
					return queryBuilder.whereLessOrEqualThan(key, value)
				} else {
					throw new Error('Invalid Search request.')
				}
			default:
				return queryBuilder
		}
	}

	private handleDirection(
		queryBuilder: IQueryBuilder<PersistableEvent>,
		attribute: EventSearchSortableAttribute
	): IQueryBuilder<PersistableEvent> {
		const { key, direction } = attribute
		switch (direction) {
			case 'ascending':
				return queryBuilder.orderByAscending(key)
			case 'descending':
				return queryBuilder.orderByDescending(key)
			default:
				return queryBuilder
		}
	}

	async search(searchQuery: List<EventSearchSortableAttribute>): Promise<List<PersistableEvent>> {
		const completeQueryBuilder = searchQuery.reduce(
			(queryBuilder, attribute) => this.handleDirection(this.handleWhere(queryBuilder, attribute), attribute),
			this.limit(1000)
		)
		return List(await completeQueryBuilder.find())
	}
}
