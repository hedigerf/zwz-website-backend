export interface HasId<T> {
	id: T
	lastModified: number
}

export interface IRepository<T extends HasId<U>, U> {
	create(event: T): Promise<T>

	update(event: T): Promise<T>

	findById(id: U): Promise<T>

	delete(id: U): Promise<void>
}

export interface LanguageHolder {
	en: string | Array<string>
	de: string | Array<string>
}
